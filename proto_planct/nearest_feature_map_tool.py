# -*- coding: utf-8 -*-
"""
/***************************************************************************
 protoplanct
                                 A QGIS plugin
 A small plugin to visualize and resample netcdf data. Originally designed to help sea biology researchers
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2022-04-06
        copyright            : (C) 2022 by BBourgoin YIkkene HMeriche ANghien 
        email                : baptiste850@gmail.com 
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 A custom QgisMapTool to extract data from raster in Qgis when the cursor is moving over pixels in the canvas
"""
from qgis.gui import QgsMapTool
from qgis.core import QgsMapLayer, QgsMapToPixel, QgsFeature, QgsFeatureRequest, QgsGeometry,QgsRaster, QgsExpressionContextUtils
from PyQt5.QtGui import QCursor, QPixmap
from PyQt5.QtCore import Qt
from qgis.PyQt.QtCore import pyqtSignal
import matplotlib.pyplot as plt
import math as m
import numpy as np


class NearestFeatureMapTool(QgsMapTool):
    
    #signal = pyqtSignal()

    def __init__(self, canvas, iface):
        
        super(QgsMapTool, self).__init__(canvas)
        self.canvas = canvas
        self.cursor = QCursor(Qt.CrossCursor)
        self.iface = iface
        self.bands= list()
        self.values= list()
        self.increment = 0
        self.firstclick = None
        self.lastclick = None

    
    def activate(self):
        self.canvas.setCursor(self.cursor)

    def canvasMoveEvent(self,mouseEvent):
        # Overwright the method from mother class, fetching data in all active rasterLayers loaded in the Qgis canvas each time the mouse is moved over it.

        #Fetch each layer 
        layers = self.canvas.layers()
        layer_names = []
        dict_list = []
        a=1
       
        try :
            for layer in layers : 
                #For each rasterLayer, get the value of every band underneath the cursor and stock it in a dict 
                a=0    
                layerPoint = self.toLayerCoordinates(layer, mouseEvent.pos())
                ident = layer.dataProvider().identify(layerPoint,QgsRaster.IdentifyFormatValue)
                dict_band = ident.results()
                #the dict and associated name are then placed in lists
                dict_list.append(dict_band)
                layer_names.append(layer.name())
        
        except:
            return
        if a==0:
            #previous lists are then passed to global variables to be accessed by the main plugin class
            #Note: there might be a "cleaner" version of doing this, yet it works
            QgsExpressionContextUtils.setGlobalVariable('layer_names',layer_names)
            QgsExpressionContextUtils.setGlobalVariable('layers',dict_list)
        
        #refresh the canvas to be detected by the main class and trigger the plot of the values in the visualisation part of the gui
        self.canvas.refresh()

        
    
        
        