﻿# **Tutoriel d’utilisation**


Le plugin Protoplanct a pour objectif la visualisation et le rééchantillonnage multi-variables environnementales de fichiers netcdf.


_Pour installer le plugin, se référer au REAME.md._

### </br>**Ce plugin ne supporte que les fichiers au format netcdf (.nc).**

### </br>**ATTENTION : Ce plugin est en version expérimentale. Des erreurs liées aux exeptions (notamment manque de rasters, ou ajout trop rapide de plusieurs fichiers dans QGIS) peuvent faire buguer le plugin. _NE PAS HESITER A RECHARGER LE PLUGIN VIA LA FONCTION PLUGIN RELOADER DE QGIS._ Une fois les bonnes options sélectionnées et les rasters ouverts, un simple rechargement du plugin le fera fonctionner normalement.**


# </br> **Fonctionnalités principales**
* Rééchantillonner temporellement des fichiers netcdf. Certains fichiers ont des dimensions temporelles mensuelles, hebdomadaires ou encore journalières. Le but de cette fonctionnalité est de les rendre compatibles afin de les afficher dynamiquement ensemble sur un même graphique.
* Afficher dynamiquement ces variables selon deux modes de parcours
* Afficher une ou plusieurs variables, modulant l’axe des ordonnées


# </br> **Interface du plugin**
Le plugin est divisé en plusieurs onglets :
* Rééchantillonnage
   * Pour objectif de prendre un fichier netcdf en entrée, en lui donnant une fréquence d’échantillonnage, la variable à échantillonner ainsi qu’un fichier en sortie.
* Visualisation
   * Pour visualiser les séries temporelles des fichiers netcdf ouverts dans QGIS.


# </br> **Rééchantillonnage**
_A partir d’un fichier en entrée, d’une fréquence d’échantillonnage et de la variable de travail, le plugin renvoie un nouveau fichier netcdf rééchantillonné._


</br>Sur cet onglet, il faut choisir les différentes options proposées.


### </br>**Raster**
* Sélectionner ici le chemin du raster en entrée.
   * **Automatiquement, les rasters ouverts dans QGIS seront proposés ici : il ne reste qu’à en sélectionner un.**


### </br>**Fréquence**
* Menu déroulant proposant différentes fréquences de rééchantillonnage (6mois, 4mois, 3mois, 2mois, mensuel, 15jours, hebdomadaire).


### </br>**Variable**
* Variable à rééchantillonner.
* Pour les fichiers multivariables, cela permet de sélectionner la variable à rééchantillonner.


### </br>**Fichier de sortie**
* Chemin du fichier en sortie d’algorithme (en .nc)


### </br>**Bouton Fast**
Le plugin propose deux modes de calcul :
* **Mode “Fast”**
   * Calcul très rapide (10x plus que l’autre mode)
   * Les données rééchantillonnées sont stockées en mémoire vive le temps du calcul pour ensuite les écrire sur le disque une seule fois.
   * **ATTENTION** : Pour les fichiers volumineux, prévoir une mémoire RAM suffisante.
* **Mode non “Fast”**
   * Calcul très lent (10x plus lent que le mode fast)
   * Les données rééchantillonnées sont écrites au fur et à mesure sur le disque : aucun problème de mémoire vive.


### </br>**Bouton GO**
Permet de lancer le calcul.




### </br> **Informations importantes**
Le rééchantillonnage utilise une méthode d’interpolation quadratique pour une meilleure précision. Il interpole à partir du fichier en entrée, pour chaque petite zone de carte (définie par la résolution latitude/longitude), selon la dimension temporelle. 
Un rééchantillonnage à la même résolution temporelle que le fichier en entrée **permet d’interpoler les valeurs manquantes du fichier**.
Les zones qui ne contiennent aucune valeur sur toute leur temporalité sont remplies par des numpy.nan.
Les zones qui contiennent _a minima_ deux valeurs non nulles sur leur dimension temporelle, interpolent les valeurs manquantes. **Ainsi, si sur un fichier de résolution mensuelle, il n’y a qu’une information en juin, tous les mois de l’année seront interpolés à partir de cette valeur.** Cette fonctionnalité a été implémentée pour permettre de retirer toutes les valeurs nan, au risque de fausser certaines zones.
        
### </br>**ATTENTION : Ce programme de rééchantillonnage ne fonctionne qu’avec des fichiers dont la durée s’étale sur une année.**


# </br>Visualisation
_Cet onglet permet de visualiser les séries temporelles des variables environnementales, selon deux affichages bien distincts._ 


### </br> **Fonctionnement**
* Sélectionner un raster dans QGIS, et passer le curseur de la souris lentement. 
* Regarder le graphique : la courbe de la série temporelle du pixel sur lequel est le curseur s’affiche. **Si plusieurs rasters sont ouverts, toutes les courbes s’afficheront.**
* Il est possible de moduler le minimum et le maximum de l’axe des ordonnées en rentrant des valeurs dans les champs Ymin et Ymax.

### </br> **Plot**
* Le graphique du haut affiche les séries temporelles en valeurs brutes. Il n'y a pas d'unité affichée car les fichiers netcdf en entrée ne contiennent pas tous des unités en documentation.
* L'axe des abscisses est sur une durée totale d'un an. Pour échelle le nombre d'éléments présents en un an.
* L'axe des ordonnées n'a pas réellement d'unité. S'il n'y a qu'un seul fichier affiché, l'unité sera celle du fichier. S'il y a plusieurs fichiers, il s'agit uniquement d'une échelle numérique, indiquant les valeurs brutes.

### </br> **Plot de Normalisation**
_Il s'agit d'un graphique qui normalise toutes les variables, permettant de façon **qualitative** d'observer visuellement une éventuelle corrélation entre ces dernières._
* Chaque variable est normalisée par rapport à elle-même.

### </br>**ATTENTION : Observer des croisements entre les différentes variables ne signifie absolument pas que de façon quantitative les valeurs de variables se croisent. Les normalisation étant différentes pour chaque variable présente, il n'y a aucun sens physique à les comparer quantitativement.**