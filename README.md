﻿# PlancTool
## Bienvenue dans ce document relatif à l’explication d’installation et de mise en place du plugin QGIS PlancTool.
Planctool est un plugin QGIS permettant le rééchantillonnage et la visualisation de données netcdf selon leur série temporelle.   

### <br/>**Fonctionnalités :**
 - Réchantillonner temporellement des données netcdf de variables environnemantales (hebdomadaire, mensuel, annuel...)
 - Afficher dynamiquement des séries temporelles de plusieurs variables et fichiers en même temps
 - Parcours des données pixel par pixel ou par régionalisation



 
### <br/>**Prérequis :**
* QGIS Version 3 (ou plus récent) installé à l’aide d’OSGeo4W
* OSGeo4W Shell
 
 
_Avant de procéder à l’installation du plugin, il faut installer les extensions python propres au plugin dans QGIS._
 
 
# <br/>**Installation des modules python**
* Ouvrir OSGeo4W Shell (l’invite de commande)
**ATTENTION : Si vous possédez plusieurs versions de QGIS, bien vérifier que vous ouvrez la version d’OSGeo4W correspondant à votre version courante de QGIS. Les modules python s'installent dans la version spécifique du shell.**
* Taper les commandes :
```
python-qgis -m pip install netCDF4
python-qgis -m pip install scipy
python-qgis -m pip install datetime
```

 
# <br/>**Installation du plugin**
* Récupérer le .zip du plugin
* Récupérer le dossier du plugin “proto-planct”
* Déposer ce dossier dans les dossier de QGIS (Généralement, chemin semblable sous Windows à : C:\Users\...\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
 
 
# <br/>**Récupération et lancement du plugin**
* Lancer QGIS (la version dans laquelle sont installés les modules pythons - correspondant à celle d’OSGeo4W)
* Onglet Extensions -> Installer/Gérer les extensions
* Onglet “Installées” -> Cocher Proto Planct _s’il n’est pas déjà coché_
 
 
Pour lancer le plugin, il suffit de cliquer sur son icône dans la barre des outils QGIS.
